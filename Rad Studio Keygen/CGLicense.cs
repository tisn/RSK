﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Rad_Studio_Keygen
{
    public class CGLicense
    {
        public static CGLicense FromEncryptedFile(string fileName)
        {
            var bytes = RadKeygen.DeCryptoFile(fileName, RadKeygen.GetKey());
            var str = Encoding.ASCII.GetString(bytes);
            return new CGLicense(str);
        }
        List<Dictionary<string, string>> Pairs = new List<Dictionary<string, string>>();
        string Line1 = "";
        public string RegistrationCode { get; private set; }
        public CGLicense(string str)
        {
            var lines = str.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            Line1 = lines[0];
            var i = 1;
            while (i < lines.Length - 3)
            {
                var c = Convert.ToInt32(lines[i]);
                var dict = new Dictionary<string, string>();
                var j = i + 1;
                for (; j < i + 1 + c * 2; j += 2)
                {
                    dict.Add(lines[j], lines[j + 1]);
                }
                i = j;
                Pairs.Add(dict);
            }
            RegistrationCode = lines[lines.Length - 2].Split('$')[1];
        }
    }
}
