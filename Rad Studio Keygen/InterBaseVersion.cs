﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rad_Studio_Keygen
{
    [JsonObject]
    public struct InterBaseVersion
    {
        public string Name;
        public string Version;
        public string IBVersion;

        public static List<InterBaseVersion> InterBaseVersionList = new List<InterBaseVersion>();

        static InterBaseVersion()
        {
            var json = JsonConvert.DeserializeObject<JObject>(Encoding.UTF8.GetString(Properties.Resources.InterBaseVersion));
            InterBaseVersionList = json["InterBaseVersions"].ToObject<List<InterBaseVersion>>();
        }
    }
}
