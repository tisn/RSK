﻿namespace Rad_Studio_Keygen
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.cbb_Version = new System.Windows.Forms.ComboBox();
            this.tb_SN = new System.Windows.Forms.TextBox();
            this.tb_RegCode = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_InstallPatch = new System.Windows.Forms.Button();
            this.btn_GenSN = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // cbb_Version
            // 
            this.cbb_Version.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_Version.FormattingEnabled = true;
            this.cbb_Version.Location = new System.Drawing.Point(131, 233);
            this.cbb_Version.Name = "cbb_Version";
            this.cbb_Version.Size = new System.Drawing.Size(303, 20);
            this.cbb_Version.TabIndex = 2;
            this.cbb_Version.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // tb_SN
            // 
            this.tb_SN.Location = new System.Drawing.Point(131, 263);
            this.tb_SN.Name = "tb_SN";
            this.tb_SN.ReadOnly = true;
            this.tb_SN.Size = new System.Drawing.Size(303, 21);
            this.tb_SN.TabIndex = 3;
            // 
            // tb_RegCode
            // 
            this.tb_RegCode.Location = new System.Drawing.Point(131, 294);
            this.tb_RegCode.Name = "tb_RegCode";
            this.tb_RegCode.ReadOnly = true;
            this.tb_RegCode.Size = new System.Drawing.Size(303, 21);
            this.tb_RegCode.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(179, 356);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "生成Slip文件";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(312, 356);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(127, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "生成SHFolder.dll";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 237);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 12);
            this.label1.TabIndex = 9;
            this.label1.Text = "Rad Studio 版本:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 268);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 12);
            this.label2.TabIndex = 9;
            this.label2.Text = "序列号:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 299);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "注册码:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btn_InstallPatch
            // 
            this.btn_InstallPatch.Location = new System.Drawing.Point(17, 356);
            this.btn_InstallPatch.Name = "btn_InstallPatch";
            this.btn_InstallPatch.Size = new System.Drawing.Size(75, 23);
            this.btn_InstallPatch.TabIndex = 10;
            this.btn_InstallPatch.Text = "安装补丁";
            this.btn_InstallPatch.UseVisualStyleBackColor = true;
            this.btn_InstallPatch.Click += new System.EventHandler(this.button4_Click);
            // 
            // btn_GenSN
            // 
            this.btn_GenSN.Location = new System.Drawing.Point(98, 356);
            this.btn_GenSN.Name = "btn_GenSN";
            this.btn_GenSN.Size = new System.Drawing.Size(75, 23);
            this.btn_GenSN.TabIndex = 11;
            this.btn_GenSN.Text = "生成序列号";
            this.btn_GenSN.UseVisualStyleBackColor = true;
            this.btn_GenSN.Click += new System.EventHandler(this.button5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(392, 382);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 12);
            this.label4.TabIndex = 13;
            this.label4.Text = "by tisn";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(12, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(422, 202);
            this.richTextBox1.TabIndex = 15;
            this.richTextBox1.Text = "";
            // 
            // FormMain
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 406);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.cbb_Version);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_SN);
            this.Controls.Add(this.btn_GenSN);
            this.Controls.Add(this.tb_RegCode);
            this.Controls.Add(this.btn_InstallPatch);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rad Studio Keygen";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.FormMain_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.FormMain_DragEnter);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.FormMain_MouseDoubleClick);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cbb_Version;
        private System.Windows.Forms.TextBox tb_SN;
        private System.Windows.Forms.TextBox tb_RegCode;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_InstallPatch;
        private System.Windows.Forms.Button btn_GenSN;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}

