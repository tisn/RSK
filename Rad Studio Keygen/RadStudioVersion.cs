﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rad_Studio_Keygen
{
    [JsonObject]
    public struct RadStudioVersion
    {
        [JsonProperty("Name")]
        public string Name { get; private set; }

        [JsonProperty("Ver")]
        public string Ver { get; private set; }

        [JsonProperty("BDSVersion")]
        public string BDSVersion { get; private set; }

        [JsonProperty("LicVerStr")]
        public string LicVerStr { get; private set; }

        [JsonProperty("LicHostPID")]
        public int LicHostPID { get; private set; }
        [JsonProperty("LicHostSKU")]
        public int LicHostSKU { get; private set; }
        [JsonProperty("LicDelphiPID")]
        public int LicDelphiPID { get; private set; }
        [JsonProperty("LicCBuilderPID")]
        public int LicCBuilderPID { get; private set; }
        [JsonProperty("BdsPatchInfo")]
        public PatchInfo BdsPatchInfo { get; private set; }
        [JsonProperty("LicenseManagerPatchInfo")]
        public PatchInfo LicenseManagerPatchInfo { get; private set; }
        [JsonProperty("mOasisRuntimePatchInfo")]
        public PatchInfo mOasisRuntimePatchInfo { get; private set; }
        [JsonProperty("SetupGUID")]
        public string SetupGUID { get; private set; }
        [JsonProperty("ISOUrl")]
        public string ISOUrl { get; private set; }
        [JsonProperty("ISOMd5")]
        public string ISOMd5 { get; private set; }
        [JsonProperty("SetupProcessName")]
        public string SetupProcessName { get; private set; }

        public static List<RadStudioVersion> RadStudioVersionList = new List<RadStudioVersion>();


        static RadStudioVersion()
        {
            var json = JsonConvert.DeserializeObject<JObject>(Encoding.UTF8.GetString(Properties.Resources.RadStudioVersion));
            RadStudioVersionList = json["RadStudionVersions"].ToObject<List<RadStudioVersion>>();
        }
    }

    [JsonObject]
    public struct PatchInfo
    {
        [JsonProperty("Crc")]
        public uint Crc;
        [JsonProperty("Sha1")]
        public string Sha1;
        [JsonProperty("PatchOffset")]
        public uint PatchOffset;
        [JsonProperty("FinalizeArrayOffset")]
        public uint FinalizeArrayOffset;
    }
}
