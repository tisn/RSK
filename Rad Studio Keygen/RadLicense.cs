﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Rad_Studio_Keygen
{
    public class RadLicense
    {
        [JsonObject]
        public class LicenseSign
        {
            [JsonProperty("e.pkg")]
            public string e_pkg = "";
            [JsonProperty("e.pt")]
            public int e_pt = 10;
            [JsonProperty("e.sign")]
            public string e_sign = "0";
            [JsonProperty("e.sign2")]
            public string e_sign2 = "0";
            [JsonProperty("e.sign3")]
            public string e_sign3 = "0";
            [JsonProperty("export.allowed")]
            public int export_allowed = 0;
            [JsonProperty("import.allowed")]
            public int import_allowed = 1;
            [JsonProperty("import.silent")]
            public int import_silent = 1;
            [JsonProperty("licensed.serialno")]
            public string licensed_serialno;
            [JsonProperty("nodelock.node")]
            public int nodelock_node = 0;
            [JsonProperty("nodelock.session")]
            public string nodelock_session = "0";
        }
        [JsonObject]
        public class LicenseInfo
        {
            public string active = "T";
            public int beta = 0;
            public int exportable = 0;
            public int hostpid = 0;
            public int hostskuid = 52;
            public int internaluse = 0;
            public int naggy = 0;
            public int noncommercial = 0;
            public string noncommercial_label = "No";
            public int platform = 1;
            public string platform_label = "Windows";
            public int product = 0;
            public int productid = 0;
            public string productid_label = "";
            public int productsku = 0;
            public string productsku_label = "";
            public string rndkey = "";
            public string serialno = "";
            public int sku = 52;
            public int templicense = 0;
            public int termtype = 0;
            public string termtype_label = "Unlimited";
            public string title = "";
            public int trial = 0;
            public int upgrade = 0;
            public int version = 21;
            public long xpdate = 0;

        }
        [JsonObject]
        public class ProductInfo
        {
            public string Android = "T";
            public string Android64 = "T";
            public string DESIGNDIAGRAMS = "TRUE";
            public string DESIGNPROJECTS = "TRUE";
            public string Desktop = "T";
            public string FULLQA = "True";
            public long FirstRegistered = 0;
            public string FulliOS = "T";
            public string Linux64 = "T";
            public string MODELLING = "True";
            public string Mobile = "T";
            public string OSX32 = "T";
            public string OSX64 = "T";
            public string Win32 = "T";
            public string Win64 = "T";
            public string a100 = "MakeThingsHappen";
            public string a1000 = "PrintMoreMoney";
            public string a101 = "ImGivinItAllShesGot";
            public string a200 = "StampIt";
            public string a250 = "ItsToolTimeBaby";
            public string a300 = "TheMalteseFalcon";
            public string a301 = "GlueSolvent";
            public string aalevel = "Custom";
            public int contactid = 13348941;
            public long crd = 1690375149000;
            public int hostsuite = 8222;
            public string iOSDevice = "T";
            public string iOSDevice32 = "T";
            public string iOSDevice64 = "T";
            public string iOSSimulator = "T";
            [JsonProperty("sub.expdate")]
            public long sub_expdate = 32503651199000; // 2999-12-31 23:59:59
            public double updatelevel = 0.0;
            public int updates = 1;
        }
        [JsonObject]
        public class SingleLicense
        {
            public LicenseInfo License = new LicenseInfo();
            public ProductInfo Product = new ProductInfo();

        }
        public List<Dictionary<string, string>> Pairs = new List<Dictionary<string, string>>();
        public LicenseSign Sign;

        public SingleLicense DelphiLicense;
        public SingleLicense DelphiLSPLicense;
        public SingleLicense CBuilderLicense;
        public SingleLicense InterBaseLicense;

        public List<SingleLicense> Licenses = new List<SingleLicense>();

        public RadLicense()
        {
            DelphiLicense = new SingleLicense();
            DelphiLSPLicense = new SingleLicense();
            CBuilderLicense = new SingleLicense();
            InterBaseLicense = new SingleLicense();

            Sign = new LicenseSign();
        }
        public RadLicense(string str)
        {
            var lines = str.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
            var i = 0;

            while (i < lines.Length)
            {
                bool first = i == 0;
                var l = lines[i];
                if (l.Trim() == "")
                    break;
                var c = Convert.ToInt32(lines[i]);
                var dict = new Dictionary<string, string>();
                var j = i + 1;
                for (; j < i + 1 + c * 2; j += 2)
                {
                    dict.Add(lines[j], lines[j + 1]);
                }
                i = j;
                Pairs.Add(dict);
                if (first)
                    i++; // 这行是表示后面有多少组
            }

            var s = JsonConvert.SerializeObject(Pairs[0]);
            Sign = JsonConvert.DeserializeObject<LicenseSign>(s);

            var lic = JsonConvert.SerializeObject(Pairs[1]);
            var pro = JsonConvert.SerializeObject(Pairs[2]);
            var li = JsonConvert.DeserializeObject<LicenseInfo>(lic);
            var pi = JsonConvert.DeserializeObject<ProductInfo>(pro);

            DelphiLicense = new SingleLicense() { License = li, Product = pi };

            DelphiLSPLicense = new SingleLicense();

            lic = JsonConvert.SerializeObject(Pairs[3]);
            pro = JsonConvert.SerializeObject(Pairs[4]);
            li = JsonConvert.DeserializeObject<LicenseInfo>(lic);
            pi = JsonConvert.DeserializeObject<ProductInfo>(pro);

            CBuilderLicense = new SingleLicense() { License = li, Product = pi };

            lic = JsonConvert.SerializeObject(Pairs[5]);
            pro = JsonConvert.SerializeObject(Pairs[6]);
            li = JsonConvert.DeserializeObject<LicenseInfo>(lic);
            pi = JsonConvert.DeserializeObject<ProductInfo>(pro);

            InterBaseLicense = new SingleLicense() { License = li, Product = pi };
        }

        public bool LDelphi = true;
        public bool LDelphiLSP = true;
        public bool LCBuilder = true;
        public bool LInterBase = true;

        public enum TermType
        {
            /// <summary>
            /// 企业版
            /// </summary>
            [EnumMember(Value = "Architect")]
            Architect = 52,
            /// <summary>
            /// 架构师
            /// </summary>
            [EnumMember(Value = "Architect")]
            Workstation = 53,
            /// <summary>
            /// 终极版
            /// </summary>
            [EnumMember(Value = "Ultimate")]
            Ultimate
        }

        /// productid
        /// product productid   
        /// 2000    2028           Delphi
        /// 4000    4028
        /// 7000    7028

        public string Generate(string SerialNumber, string ActiveCode, RadStudioVersion RadStudioVersion, TermType termType = TermType.Architect)
        {
            var rndkey = GetRndKey();
            var verNum = Convert.ToInt32(RadStudioVersion.Ver.Split('.')[0]);
            var pid = RadStudioVersion.LicHostPID;
            //var skuid = RadStudioVersion.LicHostSKU;
            var skuid = (int)termType;
            var sku_label = Enum.GetName(typeof(TermType), termType);

            Sign.e_pkg = $"Embarcadero RAD Studio {RadStudioVersion.LicVerStr} {sku_label}";
            Sign.licensed_serialno = SerialNumber;
            Sign.nodelock_session = ActiveCode;

            long xpdate = 32503651199000;
            long firstRegistered = 1355932800000;
            if (LDelphi)
            {
                // Delphi
                DelphiLicense.License.hostpid = pid;
                DelphiLicense.License.hostskuid = skuid;
                DelphiLicense.License.product = 2000;
                DelphiLicense.License.productid = RadStudioVersion.LicDelphiPID;
                DelphiLicense.License.productid_label = $"Delphi {RadStudioVersion.LicVerStr}";
                DelphiLicense.License.productsku = skuid;
                DelphiLicense.License.productsku_label = sku_label;
                DelphiLicense.License.rndkey = rndkey;
                DelphiLicense.License.serialno = SerialNumber;
                DelphiLicense.License.sku = skuid;
                DelphiLicense.License.title = $"Delphi {RadStudioVersion.LicVerStr} {sku_label}";
                DelphiLicense.License.version = verNum;
                DelphiLicense.License.xpdate = xpdate;

                DelphiLicense.Product.FirstRegistered = firstRegistered;
                DelphiLicense.Product.hostsuite = pid;
            }

            if (LDelphiLSP)
            {
                DelphiLSPLicense.License.hostpid = pid;
                DelphiLSPLicense.License.hostskuid = skuid;
                DelphiLSPLicense.License.product = 2110;
                DelphiLSPLicense.License.productid = 2110;
                DelphiLSPLicense.License.productid_label = $"DelphiLSP";
                DelphiLSPLicense.License.productsku = 0;
                DelphiLSPLicense.License.productsku_label = sku_label;
                DelphiLSPLicense.License.rndkey = rndkey;
                DelphiLSPLicense.License.serialno = SerialNumber;
                DelphiLSPLicense.License.sku = skuid;
                DelphiLSPLicense.License.title = $"DelphiLSP {sku_label}";
                DelphiLSPLicense.License.version = verNum;
                DelphiLSPLicense.License.xpdate = xpdate;

                DelphiLSPLicense.Product.FirstRegistered = firstRegistered;
                DelphiLSPLicense.Product.hostsuite = pid;
            }

            if (LCBuilder)
            {
                // C++Builder
                CBuilderLicense.License.hostpid = pid;
                CBuilderLicense.License.hostskuid = skuid;
                CBuilderLicense.License.product = 4000;
                CBuilderLicense.License.productid = RadStudioVersion.LicCBuilderPID;
                CBuilderLicense.License.productid_label = $"C++Builder {RadStudioVersion.LicVerStr}";
                CBuilderLicense.License.productsku = skuid;
                CBuilderLicense.License.productsku_label = sku_label;
                CBuilderLicense.License.rndkey = rndkey;
                CBuilderLicense.License.serialno = SerialNumber;
                CBuilderLicense.License.sku = skuid;
                CBuilderLicense.License.title = $"C++Builder {RadStudioVersion.LicVerStr} {sku_label}";
                CBuilderLicense.License.version = verNum;
                CBuilderLicense.License.xpdate = xpdate;

                CBuilderLicense.Product.FirstRegistered = firstRegistered;
                CBuilderLicense.Product.hostsuite = pid;
            }

            if (LInterBase)
            {
                InterBaseLicense.License.hostpid = pid;
                InterBaseLicense.License.hostskuid = skuid;
                InterBaseLicense.License.productid = 7113;
                InterBaseLicense.License.productid_label = $"InterBase 2020";
                InterBaseLicense.License.productsku = 0;
                InterBaseLicense.License.productsku_label = "Server";
                InterBaseLicense.License.rndkey = rndkey;
                InterBaseLicense.License.serialno = SerialNumber;
                InterBaseLicense.License.sku = skuid;
                InterBaseLicense.License.title = $"InterBase 2020 Server";
                InterBaseLicense.Product.hostsuite = pid;

            }
            var result = new List<string>();

            AddProperties(Sign, ref result);

            var ct = 0;
            if (LDelphi) ct++;
            if (LDelphiLSP) ct++;
            if (LCBuilder) ct++;
            if (LInterBase) ct++;
            result.Add(ct.ToString());
            if (LDelphi)
                AddLicenseProperties(DelphiLicense, ref result);
            if (LDelphiLSP)
                AddLicenseProperties(DelphiLSPLicense, ref result);
            if (LCBuilder)
                AddLicenseProperties(CBuilderLicense, ref result);
            if (LInterBase)
                AddLicenseProperties(InterBaseLicense, ref result);

            return string.Join("\r\n", result);
        }

        private void AddLicenseProperties(SingleLicense license, ref List<string> result)
        {
            AddProperties(license.License, ref result);
            AddProperties(license.Product, ref result);
        }

        private void AddProperties(object obj, ref List<string> result)
        {
            var json = JsonConvert.SerializeObject(obj);
            var jobject = JsonConvert.DeserializeObject<JObject>(json);
            result.Add(jobject.Children().Count().ToString());
            foreach (JProperty c in jobject.Children())
            {
                var v = c.Value;
                if (v != null)
                {
                    result.Add(c.Name);
                    result.Add(c.Value.ToString());
                }
            }
        }

        public static string CreateLicenseText(string SerialNumber, string ActiveCode, RadStudioVersion RadStudioVersion, TermType termType = TermType.Architect)
        {
            var random = new Random();
            var rndkey = GetRndKey();
            var verNum = Convert.ToInt32(RadStudioVersion.Ver.Split('.')[0]);
            var verNumj = verNum - 1;
            var pid = RadStudioVersion.LicHostPID.ToString();
            var skuid = RadStudioVersion.LicHostSKU.ToString();
            string Result = "";
            if (verNum > 27)
            {
                Result = $@"11
e.pkg
Embarcadero RAD Studio {RadStudioVersion.LicVerStr} Architect
e.pt
10
e.sign
0
e.sign2
0
e.sign3
0
export.allowed
0
import.allowed
1
import.silent
1
licensed.serialno
{SerialNumber}
nodelock.node
0
nodelock.session
{ActiveCode}
2
{verNumj}
active
T
beta
0
exportable
0
hostpid
{pid}
hostskuid
{skuid}
internaluse
0
naggy
0
noncommercial
0
noncommercial_label
No
platform
1
platform_label
Windows
product
2000
productid
{RadStudioVersion.LicDelphiPID}
productid_label
Delphi 11
productsku
{skuid}
productsku_label
Architect
rndkey
{rndkey}
serialno
{SerialNumber}
sku
{skuid}
templicense
0
termtype
0
termtype_label
Unlimited
title
Delphi {RadStudioVersion.LicVerStr} Architect
trial
0
upgrade
0
version
{verNum}
xpdate
1631247963000
30
Android
T
Android64
T
DESIGNDIAGRAMS
TRUE
DESIGNPROJECTS
TRUE
Desktop
T
FULLQA
TRUE
FirstRegistered
1631247963000
FulliOS
T
Linux64
T
MODELLING
TRUE
Mobile
T
OSX32
T
OSX64
T
Win32
T
Win64
T
a100
MakeThingsHappen
a1000
PrintMoreMoney
a101
ImGivinItAllShesGot
a200
StampIt
a250
ItsToolTimeBaby
a300
TheMalteseFalcon
a301
GlueSolvent
contactid
10655361
crd
1631247963000
hostsuite
{pid}
iOSDevice32
T
iOSDevice64
T
iOSSimulator
T
updatelevel
0.0
updates
1
{verNumj}
active
T
beta
0
exportable
0
hostpid
{pid}
hostskuid
{skuid}
internaluse
0
naggy
0
noncommercial
0
noncommercial_label
No
platform
1
platform_label
Windows
product
4000
productid
{RadStudioVersion.LicCBuilderPID}
productid_label
C++Builder 11
productsku
{skuid}
productsku_label
Architect
rndkey
{rndkey}
serialno
{SerialNumber}
sku
{skuid}
templicense
0
termtype
0
termtype_label
Unlimited
title
C++Builder {RadStudioVersion.LicVerStr} Architect
trial
0
upgrade
0
version
21
xpdate
1631247963000
29
Android
T
Android64
T
DESIGNDIAGRAMS
TRUE
DESIGNPROJECTS
TRUE
Desktop
T
FULLQA
TRUE
FirstRegistered
1631247963000
FulliOS
T
MODELLING
TRUE
Mobile
T
OSX32
T
OSX64
T
Win32
T
Win64
T
a100
MakeThingsHappen
a1000
PrintMoreMoney
a101
ImGivinItAllShesGot
a200
StampIt
a250
ItsToolTimeBaby
a300
TheMalteseFalcon
a301
GlueSolvent
contactid
10655361
crd
1631247963000
hostsuite
{pid}
iOSDevice32
T
iOSDevice64
T
iOSSimulator
T
updatelevel
0.0
updates
1
";
            }
            else
            {
                Result = "11\n";
                Result += "e.pkg\nEmbarcadero RAD Studio " + RadStudioVersion.LicVerStr + " Architect\n";
                Result += "e.pt\r\n10\r\n";
                Result += "e.sign\r\n0\r\n";
                Result += "e.sign2\r\n0\r\n";
                Result += "e.sign3\r\n0\r\n";
                Result += "export.allowed\n0\n";
                Result += "import.allowed\n1\n";
                Result += "import.silent\n1\n";
                Result += "licensed.serialno\n" + SerialNumber + "\n";
                Result += "nodelock.node\n0\n";
                Result += "nodelock.session\n" + ActiveCode + "\r\n";

                Result += "4\n";

                Result += "26\n";
                Result += "active\nT\n";
                Result += "beta\n0\n";
                Result += "exportable\n0\n";
                Result += "hostpid\n" + pid + "\n";
                Result += "hostskuid\n" + skuid + "\n";
                Result += "internaluse\n0\n";
                Result += "naggy\n0\n";
                Result += "noncommercial\n0\n";
                Result += "noncommercial_label\nNo\n";
                Result += "platform\n1\n";
                Result += "platform_label\nWindows\n";
                Result += "product\n2000\n";
                Result += "productid\n" + RadStudioVersion.LicDelphiPID + "\n";
                Result += "productid_label\nDelphi " + RadStudioVersion.LicVerStr + "\n";
                Result += "productsku\n" + skuid + "\n";
                Result += "productsku_label\nArchitect\n";
                Result += $"rndkey\n{rndkey}\n";
                Result += "serialno\n" + SerialNumber + "\n";
                Result += "sku\n" + skuid + "\n";
                Result += "templicense\n0\n";
                Result += "termtype\n0\n";
                Result += "termtype_label\nUnlimited\n";
                Result += "title\nDelphi " + RadStudioVersion.LicVerStr + " Architect\n";
                Result += "trial\n0\n";
                Result += "upgrade\n0\n";
                Result += "version\n27\n";
                Result += "27\n";
                Result += "Android\nT\n";
                Result += "DESIGNDIAGRAMS\nTRUE\n";
                Result += "DESIGNPROJECTS\nTRUE\n";
                Result += "Desktop\nT\n";
                Result += "FULLQA\nTRUE\n";
                Result += "FulliOS\nT\n";
                Result += "Linux64\nT\n";
                Result += "MODELLING\nTRUE\n";
                Result += "Mobile\nT\n";
                Result += "OSX32\nT\n";
                Result += "OSX64\nT\n";
                Result += "Win32\nT\n";
                Result += "Win64\nT\n";
                Result += "a100\nMakeThingsHappen\n";
                Result += "a1000\nPrintMoreMoney\n";
                Result += "a101\nImGivinItAllShesGot\n";
                Result += "a200\nStampIt\n";
                Result += "a250\nItsToolTimeBaby\n";
                Result += "a300\nTheMalteseFalcon\n";
                Result += "a301\nGlueSolvent\n";
                Result += "hostsuite\n" + pid + "\n";
                Result += "iOSDevice\nT\n";
                Result += "iOSDevice32\nT\n";
                Result += "iOSDevice64\nT\n";
                Result += "iOSSimulator\nT\n";
                Result += "updatelevel\n0.0\n";
                Result += "updates\n1\n";

                Result += "26\n";
                Result += "active\nT\n";
                Result += "beta\n0\n";
                Result += "exportable\n0\n";
                Result += "hostpid\n" + pid + "\n";
                Result += "hostskuid\n" + skuid + "\n";
                Result += "internaluse\n0\n";
                Result += "naggy\n0\n";
                Result += "noncommercial\n0\n";
                Result += "noncommercial_label\nNo\n";
                Result += "platform\n1\n";
                Result += "platform_label\nWindows\n";
                Result += "product\n4000\n";
                Result += "productid\n" + RadStudioVersion.LicCBuilderPID + "\n";
                Result += "productid_label\nC++Builder " + RadStudioVersion.LicVerStr + "\n";
                Result += "productsku\n" + skuid + "\n";
                Result += "productsku_label\nArchitect\n";               //Architect
                Result += $"rndkey\n{rndkey}\n";
                Result += "serialno\n" + SerialNumber + "\n";
                Result += "sku\n" + skuid + "\n";
                Result += "templicense\n0\n";
                Result += "termtype\n0\n";
                Result += "termtype_label\nUnlimited\n";
                Result += "title\nC++Builder " + RadStudioVersion.LicVerStr + " Architect\n";
                Result += "trial\n0\n";
                Result += "upgrade\n0\n";
                Result += "version\n20\n";
                Result += "25\n";
                Result += "Android\nT\n";
                Result += "DESIGNDIAGRAMS\nTRUE\n";
                Result += "DESIGNPROJECTS\nTRUE\n";
                Result += "Desktop\nT\n";
                Result += "FULLQA\nTRUE\n";
                Result += "FulliOS\nT\n";
                Result += "Linux64\nT\n";
                Result += "MODELLING\nTRUE\n";
                Result += "Mobile\nT\n";
                Result += "OSX32\nT\n";
                Result += "Win32\nT\n";
                Result += "Win64\nT\n";
                Result += "a100\nMakeThingsHappen\n";
                Result += "a1000\nPrintMoreMoney\n";
                Result += "a101\nImGivinItAllShesGot\n";
                Result += "a200\nStampIt\n";
                Result += "a250\nItsToolTimeBaby\n";
                Result += "a300\nTheMalteseFalcon\n";
                Result += "a301\nGlueSolvent\n";
                Result += "hostsuite\n" + pid + "\n";
                Result += "iOSDevice32\nT\n";
                Result += "iOSDevice64\nT\n";
                Result += "iOSSimulator\nT\n";
                Result += "updatelevel\n0.0\n";
                Result += "updates\n1\n";


                Result += "26\n";
                Result += "active\nT\n";
                Result += "beta\n0\n";
                Result += "exportable\n0\n";
                Result += "hostpid\n" + pid + "\n";
                Result += "hostskuid\n" + skuid + "\n";
                Result += "internaluse\n0\n";
                Result += "naggy\n0\n";
                Result += "noncommercial\n0\n";
                Result += "noncommercial_label\nNo\n";
                Result += "platform\n0\n";
                Result += "platform_label\nCross Platform\n";
                Result += "product\n7000\n";
                Result += "productid\n7113\n";
                Result += "productid_label\nInterBase 2020\n";        //InterBase XE7
                Result += "productsku\n0\n";
                Result += "productsku_label\nServer\n";               //Server
                Result += $"rndkey\n{rndkey}\n";
                Result += "serialno\n" + SerialNumber + "\n";
                Result += "sku\n0\n";
                Result += "templicense\n0\n";
                Result += "termtype\n0\n";
                Result += "termtype_label\nUnlimited\n";
                Result += "title\nInterBase 2020 Server\n";      //InterBase XE7 Server
                Result += "trial\n0\n";
                Result += "upgrade\n0\n";
                Result += "version\n8\n";
                Result += "21\n";
                Result += "changeView\n1\n";
                Result += "connectionMonitoring\n1\n";
                Result += "connectionsPerUser\n200\n";
                Result += "customVarId\n \n";
                Result += "databaseAccess\n1\n";
                Result += "dbEncryption\n1\n";
                Result += "ddlOperations\n1\n";
                Result += "devLicense\n1\n";
                Result += "externalFileAccess\n1\n";
                Result += "internetAccess\n1\n";
                Result += "languages\nALL\n";
                Result += "licensedCpus\n32\n";
                Result += "licensedUsers\n5000\n";
                Result += "nodeID\n \n";
                Result += "otwEncryption\n1\n";
                Result += "remoteAccess\n1\n";
                Result += "serverAccess\n1\n";
                Result += "togoAccess\n0\n";
                Result += "updatelevel\n0.0\n";
                Result += "useAddons\n0\n";
                Result += "version\n14.0\n";


                Result += "26\n";
                Result += "active\nT\n";
                Result += "beta\n0\n";
                Result += "exportable\n0\n";
                Result += "hostpid\n" + pid + "\n";
                Result += "hostskuid\n" + skuid + "\n";
                Result += "internaluse\n0\n";
                Result += "naggy\n0\n";
                Result += "noncommercial\n0\n";
                Result += "noncommercial_label\nNo\n";
                Result += "platform\n0\n";
                Result += "platform_label\nCross Platform\n";
                Result += "product\n7000\n";
                Result += "productid\n7113\n";
                Result += "productid_label\nInterBase 2020\n";        //InterBase XE7
                Result += "productsku\n16\n";
                Result += "productsku_label\nToGo Edition\n";               //ToGo Edition
                Result += $"rndkey\n{rndkey}\n";
                Result += "serialno\n" + SerialNumber + "\n";
                Result += "sku\n16\n";
                Result += "templicense\n0\n";
                Result += "termtype\n0\n";
                Result += "termtype_label\nUnlimited\n";
                Result += "title\nInterBase 2020 ToGo Edition\n";      //InterBase XE7 ToGo Edition
                Result += "trial\n0\n";
                Result += "upgrade\n0\n";
                Result += "version\n8\n";
                Result += "21\n";
                Result += "changeView\n1\n";
                Result += "connectionMonitoring\n1\n";
                Result += "connectionsPerUser\n200\n";
                Result += "customVarId\n \n";
                Result += "databaseAccess\n1\n";
                Result += "dbEncryption\n1\n";
                Result += "ddlOperations\n1\n";
                Result += "devLicense\n1\n";
                Result += "externalFileAccess\n1\n";
                Result += "internetAccess\n1\n";
                Result += "languages\nALL\n";
                Result += "licensedCpus\n32\n";
                Result += "licensedUsers\n5000\n";
                Result += "nodeID\n \n";
                Result += "otwEncryption\n1\n";
                Result += "remoteAccess\n1\n";
                Result += "serverAccess\n1\n";
                Result += "togoAccess\n1\n";
                Result += "updatelevel\n0.0\n";
                Result += "useAddons\n0\n";
                Result += "version\n14.0\n1\n";
                Result += "updatelevel\n0.0\n";

                Result += "26\n";
                Result += "active\nT\n";
                Result += "beta\n0\n";
                Result += "exportable\n0\n";
                Result += "hostpid\n" + pid + "\n";
                Result += "hostskuid\n" + skuid + "\n";
                Result += "internaluse\n0\n";
                Result += "naggy\n0\n";
                Result += "noncommercial\n0\n";
                Result += "noncommercial_label\nNo\n";
                Result += "platform\n1\n";
                Result += "platform_label\nWindows\n";
                Result += "product\n14100\n";
                Result += "productid\n14110\n";
                Result += "productid_label\nER/Studio 2016\n";
                Result += "productsku\n15\n";
                Result += "productsku_label\nDeveloper MultiPlatform\n"; //Developer MultiPlatform
                Result += $"rndkey\n{rndkey}\n";
                Result += "serialno\n" + SerialNumber + "\n";
                Result += "sku\n15\n";
                Result += "templicense\n0\n";
                Result += "termtype\n0\n";
                Result += "termtype_label\nUnlimited\n";
                Result += "title\nER/Studio Developer 2016\n";
                Result += "trial\n0\n";
                Result += "upgrade\n0\n";
                Result += "version\n10\n";
                Result += "30\n";
                Result += "CaErwin3Erx\nT\n";
                Result += "CaErwin3ErxImport.MicrosoftOfficeVisio\nT\n";
                Result += "CaErwin4Xml\nT\n";
                Result += "CaErwin7ModelManager\nT\n";
                Result += "CaErwin7Xml\nT\n";
                Result += "CaErwin8ModelManager\nT\n";
                Result += "CaErwin8Xml\nT\n";
                Result += "CaErwin9Mart\nT\n";
                Result += "CaErwin9Xml\nT\n";
                Result += "CaMart9\nT\n";
                Result += "CaModelManager7\nT\n";
                Result += "CaModelManager8\nT\n";
                Result += "CrossPlatform\nT\n";
                Result += "IbmInfoSphereDiscoveryImport.CaErwinDataProfiler\nT\n";
                Result += "IbmRationalDataArchitect\nT\n";
                Result += "IbmRationalDataArchitectImport.IbmDb2DataWarehouse\nT\n";
                Result += "IbmRationalRose98Import.MicrosoftVisualStudioModeler\nT\n";
                Result += "OmgCwmXmiImport.CaErwinDataProfiler\nT\n";
                Result += "OmgUml2XmiImport.PowerDesignerOom\nT\n";
                Result += "OmgUml2XmiImport.SparxEA\nT\n";
                Result += "OmgUmlXmiImport.SparxEA\nT\n";
                Result += "OmgUmlXmiImport.SybasePowerDesignerOom\nT\n";
                Result += "OracleDesigner\nT\n";
                Result += "SybasePowerDesignerCdm06\nT\n";
                Result += "SybasePowerDesignerCdm07\nT\n";
                Result += "SybasePowerDesignerLdm15\nT\n";
                Result += "SybasePowerDesignerPdm07\nT\n";
                Result += "baseLicense\nDeveloper\n";
                Result += "updatelevel\n0.0\n";
                Result += "updates\n1\n";


                Result += "26\n";
                Result += "active\nT\n";
                Result += "beta\n0\n";
                Result += "exportable\n0\n";
                Result += "hostpid\n" + pid + "\n";
                Result += "hostskuid\n" + skuid + "\n";
                Result += "internaluse\n0\n";
                Result += "naggy\n0\n";
                Result += "noncommercial\n0\n";
                Result += "noncommercial_label\nNo\n";
                Result += "platform\n1\n";
                Result += "platform_label\nWindows\n";
                Result += "product\n2700\n";
                Result += "productid\n2705\n";
                Result += "productid_label\nHTML5 Builder\n";         //HTML5 Builder
                Result += "productsku\n0\n";
                Result += "productsku_label\nRadPHP\n";               //RadPHP
                Result += $"rndkey\n{rndkey}\n";
                Result += "serialno\n" + SerialNumber + "\n";
                Result += "sku\n0\n";
                Result += "templicense\n0\n";
                Result += "termtype\n0\n";
                Result += "termtype_label\nUnlimited\n";
                Result += "title\nHTML5 Builder\n";                    //HTML5 Builder
                Result += "trial\n0\n";
                Result += "upgrade\n0\n";
                Result += "version\n5\n";
                Result += "1\n";
                Result += "updatelevel\n0.0\n";
            }

            return Result;
        }


        static uint randomBase = 0x0D138F2E3;
        public static uint RandomNumber()
        {
            randomBase = 0x8088405 * randomBase + 1;
            return randomBase;
        }

        public static uint sub_407334(uint a1)
        {
            ulong tmp = a1 * (ulong)RandomNumber();
            return (uint)(tmp >> 32);
        }

        public static string GetRndKey()
        {
            var v3 = sub_407334(0x20) << 16;
            var v4 = sub_407334(0x20);
            return ((v4 + v3) % 89999999 + 10000000).ToString();
        }
    }
}
