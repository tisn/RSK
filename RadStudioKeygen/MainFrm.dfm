object FrmMain: TFrmMain
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Rad Studio Keygen'
  ClientHeight = 405
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 106
    Top = 373
    Width = 90
    Height = 23
    Caption = #29983#25104
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 202
    Top = 373
    Width = 90
    Height = 23
    Caption = #34917#19969
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 305
    Top = 372
    Width = 75
    Height = 25
    Caption = 'Button3'
    TabOrder = 2
    OnClick = Button3Click
  end
  object GridPanel1: TGridPanel
    Left = 8
    Top = 262
    Width = 441
    Height = 105
    Caption = 'GridPanel1'
    ColumnCollection = <
      item
        Value = 28.366762177650430000
      end
      item
        Value = 71.633237822349570000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = Label1
        Row = 0
      end
      item
        Column = 1
        Control = cbb_Version
        Row = 0
      end
      item
        Column = 0
        Control = Label2
        Row = 1
      end
      item
        Column = 1
        Control = edt_SN
        Row = 1
      end
      item
        Column = 0
        Control = Label3
        Row = 2
      end
      item
        Column = 1
        Control = edt_RegCode
        Row = 2
      end>
    RowCollection = <
      item
        Value = 33.800810838432800000
      end
      item
        Value = 34.175692558640880000
      end
      item
        Value = 32.023496602926310000
      end>
    TabOrder = 3
    DesignSize = (
      441
      105)
    object Label1: TLabel
      Left = 13
      Top = 12
      Width = 100
      Height = 13
      Alignment = taRightJustify
      Anchors = []
      AutoSize = False
      Caption = 'Rad Studio '#29256#26412':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ExplicitLeft = 1
    end
    object cbb_Version: TComboBox
      Left = 133
      Top = 8
      Width = 300
      Height = 21
      Style = csDropDownList
      Anchors = []
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnChange = cbb_VersionChange
      ExplicitLeft = 143
    end
    object Label2: TLabel
      Left = 13
      Top = 47
      Width = 100
      Height = 13
      Alignment = taRightJustify
      Anchors = []
      AutoSize = False
      Caption = #24207#21015#21495':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ExplicitLeft = 42
    end
    object edt_SN: TEdit
      Left = 133
      Top = 43
      Width = 300
      Height = 21
      Anchors = []
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      ExplicitLeft = 143
    end
    object Label3: TLabel
      Left = 13
      Top = 81
      Width = 100
      Height = 13
      Alignment = taRightJustify
      Anchors = []
      AutoSize = False
      Caption = #27880#20876#30721':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ExplicitLeft = 42
    end
    object edt_RegCode: TEdit
      Left = 133
      Top = 77
      Width = 300
      Height = 21
      Anchors = []
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      ExplicitLeft = 143
    end
  end
  object btn_InstallPatch: TButton
    Left = 8
    Top = 372
    Width = 75
    Height = 25
    Caption = #23433#35013#34917#19969
    TabOrder = 4
    OnClick = btn_InstallPatchClick
  end
  object redt1: TRichEdit
    Left = 8
    Top = 8
    Width = 441
    Height = 248
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Lines.Strings = (
      'redt1')
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 5
    Zoom = 100
  end
end
