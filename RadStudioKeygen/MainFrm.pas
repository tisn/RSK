unit MainFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, RadVersion, Vcl.ExtCtrls, Vcl.ComCtrls;

type
  TFrmMain = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    GridPanel1: TGridPanel;
    Label1: TLabel;
    cbb_Version: TComboBox;
    Label2: TLabel;
    edt_SN: TEdit;
    Label3: TLabel;
    edt_RegCode: TEdit;
    btn_InstallPatch: TButton;
    redt1: TRichEdit;
    procedure FormCreate(Sender: TObject);
    procedure cbb_VersionChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure btn_InstallPatchClick(Sender: TObject);
  private
    { Private declarations }
    function GetCurrentVersion(): PRadStudioVersion;
  public
    { Public declarations }
  end;

var
  FrmMain: TFrmMain;

implementation

uses
  RadKeygen, RTF;
{$R *.dfm}

function TFrmMain.GetCurrentVersion(): PRadStudioVersion;
begin

  Result := PRadStudioVersion(RadStudioVersionList.Objects[cbb_Version.ItemIndex]);
end;

procedure TFrmMain.btn_InstallPatchClick(Sender: TObject);
var
  cv: PRadStudioVersion;
  version: Single;
begin
  if Trim(edt_RegCode.Text) <> '' then
  begin
    cv := GetCurrentVersion;
    RadKeygen.PatchmOasisRuntime(cv);
    version := Single.Parse(cv.BDSVersion);
    if version >= 21.0 then
    begin

    end;
  end;
end;

procedure TFrmMain.Button1Click(Sender: TObject);
var
  FileName: string;
begin
  edt_SN.Text := RadKeygen.GenerateSerialNumber;
  if edt_RegCode.Text = '' then
    edt_RegCode.Text := RadKeygen.GetRegistrationCode;
  if Trim(edt_RegCode.Text) <> '' then
  begin
    if RadKeygen.GenerateLicenseFile(edt_SN.Text, edt_RegCode.Text, GetCurrentVersion, FileName) then
    begin
      MessageBox(Handle, PChar('Slip 文件保存成功!'#10 + FileName), PChar(Application.Title), MB_OK + MB_ICONINFORMATION);
    end
    else
    begin
      MessageBox(Handle, PChar('保存Slip文件时发生错误！'), PChar(Application.Title), MB_OK + MB_ICONWARNING);
    end;
  end;
end;

procedure TFrmMain.Button2Click(Sender: TObject);
var
  FileName: string;
begin
  if RadKeygen.PatchFile(GetCurrentVersion, FileName) then
  begin
    MessageBox(Handle, PChar('补丁文件已成功保存！'#10 + FileName), PChar(Application.Title), MB_OK + MB_ICONINFORMATION);
  end
  else
  begin
    MessageBox(Handle, PChar('补丁文件发生错误！'), PChar(Application.Title), MB_OK + MB_ICONWARNING);
  end;
end;

procedure TFrmMain.Button3Click(Sender: TObject);
var
  FileName: string;
begin
  RadKeygen.GenerateLicenseFile('HNFA-AAAKNK-FHABCL-FCPN', '3953031', GetCurrentVersion, FileName);
end;

procedure TFrmMain.cbb_VersionChange(Sender: TObject);
var
  HowToUse: string;
  rstream: TResourceStream;
  strStream: TStream;
  bs: TBytes;
  html: string;
begin
  rstream := TResourceStream.Create(HInstance, 'Info', RT_RCDATA);
  SetLength(bs, rstream.Size);
  rstream.Read(bs, 0, Length(bs));
  FreeAndNil(rstream);

  html := TEncoding.UTF8.GetString(bs);
  html := html.Replace('<Name>', GetCurrentVersion^.Name);
  html := html.Replace('<Ver>', GetCurrentVersion^.Ver);
  html := html.Replace('<ISOUrl>', GetCurrentVersion^.ISOUrl);
  html := html.Replace('<ISOMd5>', GetCurrentVersion^.ISOMd5);

  redt1.PlainText := False;
  strStream := TStringStream.Create(html);
  redt1.Lines.LoadFromStream(strStream);
  FreeAndNil(strStream);

  Caption := Format('Rad Studio Keygen - %s', [GetCurrentVersion^.Name]);
end;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
  cbb_Version.Items.AddStrings(RadStudioVersionList);
  if cbb_Version.Items.Count > 0 then
  begin
    cbb_Version.ItemIndex := 0;
    cbb_VersionChange(cbb_Version);
  end;
  edt_SN.Text := RadKeygen.GenerateSerialNumber;
  edt_RegCode.Text := RadKeygen.GetRegistrationCode;
end;

end.

