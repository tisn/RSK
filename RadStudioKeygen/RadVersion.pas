unit RadVersion;

interface

uses
  Windows, Classes, SysUtils;

type
  TPatchInfo = record
    Crc: DWORD;
    Sha1: string;
    PatchOffset: DWORD;
    FinalizeArrayOffset: DWORD;
  end;

  TRadStudioVersion = record
    Name: string; // Rad Studio 10.1 Berlin Update1
    Ver: string; // 24.0.24468.8770
    BDSVersion: string; // 18.0
    LicVerStr: string; // 10.1 Berlin
    LicHostPID: Integer; // 8218
    LicHostSKU: Integer; // 53
    LicDelphiPID: string;
    LicCBuilderPID: string;
    BdsPatchInfo: TPatchInfo;
    LicenseManagerPatchInfo: TPatchInfo;
    mOasisRuntimePatchInfo: TPatchInfo;
    SetupGUID: string;
    ISOUrl: string;
    ISOMd5: string;
    SetupProcessName: string;
  end;

  PRadStudioVersion = ^TRadStudioVersion;

var
  RadStudioVersionList: TStringList;

implementation

uses superobject;

procedure InitRadStudioVersion(VerList: TStringList);
var
  RadStudioVersion: PRadStudioVersion;
  rstream: TResourceStream;
  j: ISuperObject;
  RadStudionVersions: ISuperObject;
  rv: ISuperObject;
  o: TObject;
begin
  rstream := TResourceStream.Create(HInstance, 'RadStudioVersion', RT_RCDATA);
  j := TSuperObject.ParseStream(rstream, True);
  FreeAndNil(rstream);
  RadStudionVersions := j['RadStudionVersions'];
  for rv in RadStudionVersions do
  begin
     New(RadStudioVersion);
     with RadStudioVersion^ do
      begin
       Name := rv.S['Name'];
       Ver := rv.S['Ver'];
       BDSVersion := rv.S['BDSVersion'];
       LicVerStr := rv.S['LicVerStr'];
       LicHostPID := rv['LicHostPID'].AsInteger;
       LicHostSKU := rv['LicHostSKU'].AsInteger;
       LicDelphiPID := rv.S['LicDelphiPID'];
       LicCBuilderPID := rv.S['LicCBuilderPID'];

       BdsPatchInfo.Crc := rv['BdsPatchInfo'].I['Crc'];
       BdsPatchInfo.Sha1 := rv['BdsPatchInfo'].S['Sha1'];
       BdsPatchInfo.PatchOffset := rv['BdsPatchInfo'].I['PatchOffset'];
       BdsPatchInfo.FinalizeArrayOffset := rv['BdsPatchInfo'].I['FinalizeArrayOffset'];

       LicenseManagerPatchInfo.Crc := rv['LicenseManagerPatchInfo'].I['Crc'];
       LicenseManagerPatchInfo.Sha1 := rv['LicenseManagerPatchInfo'].S['Sha1'];
       LicenseManagerPatchInfo.PatchOffset := rv['LicenseManagerPatchInfo'].I['PatchOffset'];
       LicenseManagerPatchInfo.FinalizeArrayOffset := rv['LicenseManagerPatchInfo'].I['FinalizeArrayOffset'];

       mOasisRuntimePatchInfo.Sha1 := rv['mOasisRuntimePatchInfo'].S['Sha1'];
       mOasisRuntimePatchInfo.PatchOffset := rv['mOasisRuntimePatchInfo'].I['PatchOffset'];

       SetupGUID := rv.S['SetupGUID'];
       ISOUrl := rv.S['ISOUrl'];
       ISOMd5 := rv.S['ISOMd5'];
     end;
     VerList.AddObject(RadStudioVersion^.Name, TObject(RadStudioVersion));
  end;

  Exit;

  New(RadStudioVersion);
  with RadStudioVersion^ do
  begin
    Name := 'RAD Studio 10.4.1 Sydney';
    Ver := '27.0.38860.1461';
    BDSVersion := '21.0';
    LicVerStr := '10.4';
    LicHostPID := 8221;
    LicHostSKU := 52;
    LicDelphiPID := '2027';
    LicCBuilderPID := '4024';
    BdsPatchInfo.Crc := $8F7C1627;
    BdsPatchInfo.Sha1 := 'EAE45253CE6A6E7A770406A52D4F0EFBDD467128';
    BdsPatchInfo.PatchOffset := $1FBE4;
    BdsPatchInfo.FinalizeArrayOffset := $11EF7C;
    LicenseManagerPatchInfo.Crc := $5C14D67C;
    LicenseManagerPatchInfo.Sha1 := '029C2F57E3BF2456FCCAE543252083F1D1B20D73';
    LicenseManagerPatchInfo.PatchOffset := $156320;
    LicenseManagerPatchInfo.FinalizeArrayOffset := $5A07A4;
    mOasisRuntimePatchInfo.Sha1 := '101FC6D71A1DDEAF3B079477560DD0307ADE3C80';
    mOasisRuntimePatchInfo.PatchOffset := $0016CFE9;
    // SetupGUID := '{426A3606-6CB8-4CF8-87A8-44388377C47D}';
    ISOUrl := 'https://altd.embarcadero.com/download/radstudio/10.4/radstudio_10_4_101461a.iso';
    ISOMd5 := '952f87f0001d5a5e2310cbc8b5d7febf';
  end;
  VerList.AddObject(RadStudioVersion^.Name, TObject(RadStudioVersion));

  New(RadStudioVersion);
  with RadStudioVersion^ do
  begin
    Name := 'Rad Studio 10.4 Sydney Enterprise Patch 2';
    Ver := '27.0.37889.9797';
    BDSVersion := '21.0';
    LicVerStr := '10.4';
    LicHostPID := 8221;
    LicHostSKU := 52;
    LicDelphiPID := '2027';
    LicCBuilderPID := '4024';
    BdsPatchInfo.Crc := $0ACD0BB4F;
    BdsPatchInfo.Sha1 := 'd385d260685b489295d0eaf6cdf5ec60dd98b28b';
    BdsPatchInfo.PatchOffset := $1FE10;
    BdsPatchInfo.FinalizeArrayOffset := $124B1C;
    LicenseManagerPatchInfo.Crc := $0C4BDE7B6;
    LicenseManagerPatchInfo.Sha1 := '6717ee537e8c179a7039485aa75b4833b5fddfdd';
    LicenseManagerPatchInfo.PatchOffset := $156204;
    LicenseManagerPatchInfo.FinalizeArrayOffset := $5A077C;
    mOasisRuntimePatchInfo.Sha1 := '101fc6d71a1ddeaf3b079477560dd0307ade3c80';
    mOasisRuntimePatchInfo.PatchOffset := $16CFE9;
    SetupGUID := '{426A3606-6CB8-4CF8-87A8-44388377C47D}';
    ISOUrl := 'http://altd.embarcadero.com/download/radstudio/10.4/radstudio_10_4_99797b.iso';
    ISOMd5 := '952f87f0001d5a5e2310cbc8b5d7febf';
  end;
  VerList.AddObject(RadStudioVersion^.Name, TObject(RadStudioVersion));

  New(RadStudioVersion);
  with RadStudioVersion^ do
  begin
    Name := 'Rad Studio 10.4 Sydney Enterprise';
    Ver := '27.0.37889.9797';
    BDSVersion := '21.0';
    LicVerStr := '10.4';
    LicHostPID := 8221;
    LicHostSKU := 52;
    LicDelphiPID := '2027';
    LicCBuilderPID := '4024';
    BdsPatchInfo.Crc := $532F44B1;
    BdsPatchInfo.Sha1 := 'd385d260685b489295d0eaf6cdf5ec60dd98b28b';
    BdsPatchInfo.PatchOffset := $1FE10;
    BdsPatchInfo.FinalizeArrayOffset := $124B1C;
    LicenseManagerPatchInfo.Crc := $4FFA7BA0;
    LicenseManagerPatchInfo.Sha1 := 'ff6b80d7369a8fecc22895155c286e884295fcfa';
    LicenseManagerPatchInfo.PatchOffset := $156208;
    LicenseManagerPatchInfo.FinalizeArrayOffset := $5A077C;
    mOasisRuntimePatchInfo.Sha1 := '101FC6D71A1DDEAF3B079477560DD0307ADE3C80';
    mOasisRuntimePatchInfo.PatchOffset := $0016CFE9;
    SetupGUID := '{426A3606-6CB8-4CF8-87A8-44388377C47D}';
    ISOUrl := 'http://altd.embarcadero.com/download/radstudio/10.4/radstudio_10_4_99797b.iso';
    ISOMd5 := '20fd1724606290ecaba8b7b8de6ade1d ';
  end;
  VerList.AddObject(RadStudioVersion^.Name, TObject(RadStudioVersion));

  New(RadStudioVersion);
  with RadStudioVersion^ do
  begin
    Name := 'Rad Studio 10.3.1 Rio Enterprise';
    Ver := '26.0.33219.4890';
    // Edition := 'Enterprise';
    // DelphiVer := '26';
    // CBuilderVer := '23';
    // BDSVersion := '20.0';
    LicVerStr := '10.3 Rio';
    LicHostPID := 8220;
    LicHostSKU := 52;
    LicDelphiPID := '2026';
    LicCBuilderPID := '4023';
    BdsPatchInfo.Crc := $F05F3031;
    BdsPatchInfo.Sha1 := 'dae1fe4463cc70ba66a997dacfa78dc9e4ca1c56';
    BdsPatchInfo.PatchOffset := $1F728;
    BdsPatchInfo.FinalizeArrayOffset := $11EF84;
    LicenseManagerPatchInfo.Crc := $B7637921;
    LicenseManagerPatchInfo.Sha1 := 'e9f7fcae00a9cd90abcb687d1c58f9a1077cc25a';
    LicenseManagerPatchInfo.PatchOffset := $1F0484;
    LicenseManagerPatchInfo.FinalizeArrayOffset := $6237CC;
    mOasisRuntimePatchInfo.Sha1 := '101fc6d71a1ddeaf3b079477560dd0307ade3c80';
    mOasisRuntimePatchInfo.PatchOffset := $16CFE9;
    SetupGUID := '{51D553F1-B483-41C2-B35E-6D461D9E0F9C}';
    ISOUrl := 'http://altd.embarcadero.com/download/radstudio/10.3/delphicbuilder10_3_1_194899.iso';
    ISOMd5 := '32094197E03E2F18BCA1D2F612830CD4';
  end;
  VerList.AddObject(RadStudioVersion^.Name, TObject(RadStudioVersion));

  New(RadStudioVersion);
  with RadStudioVersion^ do
  begin
    Name := 'Rad Studio 10.3 Rio 4364 Architect';
    Ver := '26.0.32429.4364';
    BDSVersion := '20.0';
    LicVerStr := '10.3 Rio';
    LicHostPID := 8220;
    LicHostSKU := 53;
    LicDelphiPID := '2026';
    LicCBuilderPID := '4023';
    BdsPatchInfo.Crc := $C426EC4A;
    BdsPatchInfo.Sha1 := '43BB879FE9EFD7B8752F2B5D99DF31F7CD948D68';
    BdsPatchInfo.PatchOffset := $1F724;
    BdsPatchInfo.FinalizeArrayOffset := $11EF7C;
    LicenseManagerPatchInfo.Crc := $0D512F70;
    LicenseManagerPatchInfo.Sha1 := '8D16D4521BCC12D537EB20B33864654E7A5B81C0';
    LicenseManagerPatchInfo.PatchOffset := $1F002C;
    LicenseManagerPatchInfo.FinalizeArrayOffset := $6227A4;
    mOasisRuntimePatchInfo.Sha1 := '101FC6D71A1DDEAF3B079477560DD0307ADE3C80';
    mOasisRuntimePatchInfo.PatchOffset := $0016CFE9;
    SetupGUID := '{426A3606-6CB8-4CF8-87A8-44388377C47D}';
    ISOUrl := 'http://altd.embarcadero.com/download/radstudio/10.3/delphicbuilder10_3_0_94364.iso';
    ISOMd5 := '0882D58CB53A7D0A828CC45D06C6ECD0';
  end;
  VerList.AddObject(RadStudioVersion^.Name, TObject(RadStudioVersion));

  New(RadStudioVersion);
  with RadStudioVersion^ do
  begin
    Name := 'Rad Studio 10.2.3 Tokyo 3231';
    Ver := '25.0.31059.3231';
    BDSVersion := '19.0';
    LicVerStr := '10.2 Tokyo';
    LicHostPID := 8219;
    LicHostSKU := 52;
    LicDelphiPID := '2025';
    LicCBuilderPID := '4022';
    BdsPatchInfo.Crc := $CE8FA21E;
    BdsPatchInfo.Sha1 := '8daa98dbc558ec81cf582ec8c71233d9ab5fb76a';
    BdsPatchInfo.PatchOffset := $1E95D;
    BdsPatchInfo.FinalizeArrayOffset := $124BDC;
    LicenseManagerPatchInfo.Crc := $1127F753;
    LicenseManagerPatchInfo.Sha1 := '485dcb165cdefe3f3e50090bf8cfafb8bca5b46f';
    LicenseManagerPatchInfo.PatchOffset := $1E4939;
    LicenseManagerPatchInfo.FinalizeArrayOffset := $607828;
    mOasisRuntimePatchInfo.Sha1 := '30dc7ee5931b2f88904c60b5469144673bc544a8';
    mOasisRuntimePatchInfo.PatchOffset := $166F85;
    SetupGUID := '{15CEC4B7-6F61-4D40-9491-255657E369A2}';
    ISOUrl := 'http://altd.embarcadero.com/download/radstudio/10.2/delphicbuilder10_2_3__93231.iso';
    ISOMd5 := '40D693B9989F7CCDF07C07EA676D1AB2';
  end;
  VerList.AddObject(RadStudioVersion^.Name, TObject(RadStudioVersion));

  New(RadStudioVersion);
  with RadStudioVersion^ do
  begin
    Name := 'Rad Studio 10.2.3 Tokyo 2631';
    Ver := '25.0.29899.2631';
    BDSVersion := '19.0';
    LicVerStr := '10.2 Tokyo';
    LicHostPID := 8219;
    LicHostSKU := 52;
    LicDelphiPID := '2025';
    LicCBuilderPID := '4022';
    BdsPatchInfo.Crc := $3D387FC1;
    BdsPatchInfo.Sha1 := '111eeb9c9061b1e125318799d1b6de83ce9d2499';
    BdsPatchInfo.PatchOffset := $1E92D;
    BdsPatchInfo.FinalizeArrayOffset := $10E100;
    LicenseManagerPatchInfo.Crc := $9F380FEB;
    LicenseManagerPatchInfo.Sha1 := '9e8ad67357cbd2e2a4cc851fc2d582f7f89882ea';
    LicenseManagerPatchInfo.PatchOffset := $1E4939;
    LicenseManagerPatchInfo.FinalizeArrayOffset := $607828;
    mOasisRuntimePatchInfo.Sha1 := '30dc7ee5931b2f88904c60b5469144673bc544a8';
    mOasisRuntimePatchInfo.PatchOffset := $166F85;
    SetupGUID := '{426F14E1-A160-430C-A48D-E84ED4F49171}';
    ISOUrl := 'http://altd.embarcadero.com/download/radstudio/10.2/delphicbuilder10_2_3_2631.iso';
    ISOMd5 := '1bd28e95596ffed061e57e28e155666d';
  end;
  VerList.AddObject(RadStudioVersion^.Name, TObject(RadStudioVersion));

  New(RadStudioVersion);
  with RadStudioVersion^ do
  begin
    Name := 'Rad Studio 10.2 Tokyo Update2 2004';
    Ver := '25.0.29039.2004';
    BDSVersion := '19.0';
    LicVerStr := '10.2 Tokyo';
    LicHostPID := 8219;
    LicHostSKU := 52;
    LicDelphiPID := '2025';
    LicCBuilderPID := '4022';
    BdsPatchInfo.Crc := $F7C582B3;
    BdsPatchInfo.Sha1 := '2a93ca97525a9b426ed24250c74f1cecadd29f25';
    BdsPatchInfo.PatchOffset := $1E915;
    BdsPatchInfo.FinalizeArrayOffset := $10E0EC;
    LicenseManagerPatchInfo.Crc := $59A40772;
    LicenseManagerPatchInfo.Sha1 := '7110a5f35a721505395e075e72783d16c4e0f02f';
    LicenseManagerPatchInfo.PatchOffset := $1E47B1;
    LicenseManagerPatchInfo.FinalizeArrayOffset := $6077FC;
    mOasisRuntimePatchInfo.Sha1 := '30dc7ee5931b2f88904c60b5469144673bc544a8';
    mOasisRuntimePatchInfo.PatchOffset := $166F85;
    SetupGUID := '{62610C82-8A7D-4055-B9B4-BB8D34823B3D}';
    ISOUrl := 'http://altd.embarcadero.com/download/radstudio/10.2/delphicbuilder10_2_2_2004.iso';
    ISOMd5 := 'ac1fa2e0e9be86b5118742b782477b61';
  end;
  VerList.AddObject(RadStudioVersion^.Name, TObject(RadStudioVersion));

  New(RadStudioVersion);
  with RadStudioVersion^ do
  begin
    Name := 'Rad Studio 10.2 Tokyo Update2';
    Ver := '25.0.28979.1978';
    BDSVersion := '19.0';
    LicVerStr := '10.2 Tokyo';
    LicHostPID := 8219;
    LicHostSKU := 52;
    LicDelphiPID := '2025';
    LicCBuilderPID := '4022';
    BdsPatchInfo.Crc := $46B87B2D;
    BdsPatchInfo.Sha1 := 'a90a1143397fdef9a8abb5c7968dd1e478ebec21';
    BdsPatchInfo.PatchOffset := $1EAA1;
    BdsPatchInfo.FinalizeArrayOffset := $10F0EC;
    LicenseManagerPatchInfo.Crc := $18532E3F;
    LicenseManagerPatchInfo.Sha1 := 'ffd6b82ac163fe9fd635395cb3f3d31e5d21bf8f';
    LicenseManagerPatchInfo.PatchOffset := $1E4E21;
    LicenseManagerPatchInfo.FinalizeArrayOffset := $60B7FC;
    mOasisRuntimePatchInfo.Sha1 := '30dc7ee5931b2f88904c60b5469144673bc544a8';
    mOasisRuntimePatchInfo.PatchOffset := $166F85;
    SetupGUID := '{7927985A-BF91-45B3-866E-B275708A707E}';
    ISOUrl := 'http://altd.embarcadero.com/download/radstudio/10.2/delphicbuilder10_2_2.iso';
    ISOMd5 := '54F473B146464C4F74032A6A2527F3C3';
  end;
  VerList.AddObject(RadStudioVersion^.Name, TObject(RadStudioVersion));

  New(RadStudioVersion);
  with RadStudioVersion^ do
  begin
    Name := 'Rad Studio 10.2 Tokyo Update1';
    Ver := '25.0.27659.1188';
    BDSVersion := '19.0';
    LicVerStr := '10.2 Tokyo';
    LicHostPID := 8219;
    LicHostSKU := 52;
    LicDelphiPID := '2025';
    LicCBuilderPID := '4022';
    BdsPatchInfo.Crc := $BC350A66;
    BdsPatchInfo.Sha1 := '67a1602a64297743f12758ce74b437f596054e30';
    BdsPatchInfo.PatchOffset := $1E7D9;
    BdsPatchInfo.FinalizeArrayOffset := $10BFFC;
    LicenseManagerPatchInfo.Crc := $0C209F01;
    LicenseManagerPatchInfo.Sha1 := '65b07e3d273a3c1ec0f722266ad73375d02e69a0';
    LicenseManagerPatchInfo.PatchOffset := $001E4435;
    LicenseManagerPatchInfo.FinalizeArrayOffset := $6077D4;
    mOasisRuntimePatchInfo.Sha1 := '39ecf2e1a55c62ba56efd861d7bde7dd83f8551f';
    mOasisRuntimePatchInfo.PatchOffset := $00165FD5;
    SetupGUID := '{157FDBBC-21E6-4B45-A995-CA25BB2864BF}';
    ISOUrl := 'http://altd.embarcadero.com/download/radstudio/10.2/delphicbuilder10_2_1.iso';
    ISOMd5 := '3f7028be8d3831b098102e9bf5732e3b';
  end;
  VerList.AddObject(RadStudioVersion^.Name, TObject(RadStudioVersion));

  New(RadStudioVersion);
  with RadStudioVersion^ do
  begin
    Name := 'Rad Studio 10.2 Tokyo';
    Ver := '25.0.26309.314';
    BDSVersion := '19.0';
    LicVerStr := '10.2 Tokyo';
    LicHostPID := 8219;
    LicHostSKU := 52;
    LicDelphiPID := '2025';
    LicCBuilderPID := '4022';
    BdsPatchInfo.Crc := $23D6BBC6;
    BdsPatchInfo.Sha1 := '6b61fe60e4f806913fdf103e294200d1341089ba';
    BdsPatchInfo.PatchOffset := $1E7D5;
    BdsPatchInfo.FinalizeArrayOffset := $26886C;
    LicenseManagerPatchInfo.Crc := $34D8A9BA;
    LicenseManagerPatchInfo.Sha1 := 'a2feb2da6e8b7cc7660ed2b54eec1cad09daeb6c';
    LicenseManagerPatchInfo.PatchOffset := $001E4209;
    LicenseManagerPatchInfo.FinalizeArrayOffset := $660B6C;
    mOasisRuntimePatchInfo.Sha1 := '39ecf2e1a55c62ba56efd861d7bde7dd83f8551f';
    mOasisRuntimePatchInfo.PatchOffset := $00165FD5;
    SetupGUID := '{0556178E-2062-46E3-8FE9-E620C40DB02B}';
    ISOUrl := 'http://altd.embarcadero.com/download/radstudio/10.2/delphicbuilder10_2.iso';
    ISOMd5 := '8855db8d40993c18672f226bf395bfcd';
  end;
  VerList.AddObject(RadStudioVersion^.Name, TObject(RadStudioVersion));

  New(RadStudioVersion);
  with RadStudioVersion^ do
  begin
    Name := 'Rad Studio 10.1 Berlin Update2';
    Ver := '24.0.25048.9432';
    BDSVersion := '18.0';
    LicVerStr := '10.1 Berlin';
    LicHostPID := 8218;
    LicHostSKU := 53;
    LicDelphiPID := '2024';
    LicCBuilderPID := '4021';
    BdsPatchInfo.Crc := $A1315AAB;
    BdsPatchInfo.Sha1 := '9627eeef0574f46f4ec9348b806d30c9c37ad3ed';
    BdsPatchInfo.PatchOffset := $5143D;
    BdsPatchInfo.FinalizeArrayOffset := $264604;
    LicenseManagerPatchInfo.Crc := $9189A95C;
    LicenseManagerPatchInfo.Sha1 := '493ebece2682544c2d6c2bbbdba5b6da70ca73e1';
    LicenseManagerPatchInfo.PatchOffset := $1E82E5;
    LicenseManagerPatchInfo.FinalizeArrayOffset := $671A18;
    mOasisRuntimePatchInfo.Sha1 := '39ecf2e1a55c62ba56efd861d7bde7dd83f8551f';
    mOasisRuntimePatchInfo.PatchOffset := $00165FD5;
    SetupGUID := '{2008E4BD-A356-4759-8A78-18636D2E75C9}';
    ISOUrl := 'http://altd.embarcadero.com/download/radstudio/10.1/radstudio10_1_upd2_esd.iso';
    ISOMd5 := '920f0acf67122bb04ed55edd7a1c7579';
  end;
  VerList.AddObject(RadStudioVersion^.Name, TObject(RadStudioVersion));

  New(RadStudioVersion);
  with RadStudioVersion^ do
  begin
    Name := 'Rad Studio 10.1 Berlin Update1';
    Ver := '24.0.24468.8770';
    BDSVersion := '18.0';
    LicVerStr := '10.1 Berlin';
    LicHostPID := 8218;
    LicHostSKU := 53;
    LicDelphiPID := '2024';
    LicCBuilderPID := '4021';
    BdsPatchInfo.Crc := $9626A6DC;
    BdsPatchInfo.Sha1 := '82d3cd849786f2ece428ab7518ec9ecf47d475e6';
    BdsPatchInfo.PatchOffset := $51449;
    BdsPatchInfo.FinalizeArrayOffset := $264584;
    LicenseManagerPatchInfo.Crc := $3B314A18;
    LicenseManagerPatchInfo.Sha1 := '79b342e41f97728e16c6302e08b44f89b0655a9e';
    LicenseManagerPatchInfo.PatchOffset := $1E8FB5;
    LicenseManagerPatchInfo.FinalizeArrayOffset := $6729E0;
    mOasisRuntimePatchInfo.Sha1 := '7aa466dd1d2c685edd69ee41d1c8ebc1d2b56bb4';
    mOasisRuntimePatchInfo.PatchOffset := $00162CBD;
    SetupGUID := '{37C118B3-EF7F-4110-BFE5-E866FB456C8E}';
    ISOUrl := 'http://altd.embarcadero.com/download/radstudio/10.1/delphicbuilder10_1_upd1.iso';
    ISOMd5 := 'a85a0fba4f8bab121312184cda85c198';
  end;
  VerList.AddObject(RadStudioVersion^.Name, TObject(RadStudioVersion));

  New(RadStudioVersion);
  with RadStudioVersion^ do
  begin
    Name := 'Rad Studio 10.1 Berlin';
    Ver := '24.0.22858.6822';
    BDSVersion := '18.0';
    LicVerStr := '10.1 Berlin';
    LicHostPID := 8218;
    LicHostSKU := 53;
    LicDelphiPID := '2024';
    LicCBuilderPID := '4021';
    BdsPatchInfo.Crc := $1BA3E394;
    BdsPatchInfo.Sha1 := 'a492883335230bced0651338584fbe8c49bd94a8';
    BdsPatchInfo.PatchOffset := $51449;
    BdsPatchInfo.FinalizeArrayOffset := $264584;
    LicenseManagerPatchInfo.Crc := $D2BAA257;
    LicenseManagerPatchInfo.Sha1 := 'd0d024b97d02608a505fb0e667dd564b53c91b13';
    LicenseManagerPatchInfo.PatchOffset := $1E9035;
    LicenseManagerPatchInfo.FinalizeArrayOffset := $6719B8;
    mOasisRuntimePatchInfo.Sha1 := '7aa466dd1d2c685edd69ee41d1c8ebc1d2b56bb4';
    mOasisRuntimePatchInfo.PatchOffset := $00162CBD;
    SetupGUID := '{655CBACE-A23C-42B8-B924-A88E80F352B5}';
    ISOUrl := 'http://altd.embarcadero.com/download/radstudio/10.1/delphicbuilder10_1.iso';
    ISOMd5 := '466d2db93e5b3b631eabba69d052b28f';
  end;
  VerList.AddObject(RadStudioVersion^.Name, TObject(RadStudioVersion));

  New(RadStudioVersion);
  with RadStudioVersion^ do
  begin
    Name := 'Rad Studio 10 Seattle Update1';
    Ver := '23.0.21418.4207';
    BDSVersion := '17.0';
    LicVerStr := '10 Seattle';
    LicHostPID := 8217;
    LicHostSKU := 53;
    LicDelphiPID := '2023';
    LicCBuilderPID := '4020';
    BdsPatchInfo.Crc := $B5BD665F;
    BdsPatchInfo.Sha1 := 'e8cc301efc449f90750d921ab73be31d824c08c6';
    BdsPatchInfo.PatchOffset := $4FE51;
    BdsPatchInfo.FinalizeArrayOffset := $225F84;
    LicenseManagerPatchInfo.Crc := $8395454D;
    LicenseManagerPatchInfo.Sha1 := '0ca4640d6c1c2f470ff3182809b881a97e76e534';
    LicenseManagerPatchInfo.PatchOffset := $1CA696;
    LicenseManagerPatchInfo.FinalizeArrayOffset := $6306AC;
    mOasisRuntimePatchInfo.Sha1 := '7aa466dd1d2c685edd69ee41d1c8ebc1d2b56bb4';
    mOasisRuntimePatchInfo.PatchOffset := $00162CBD;
    SetupGUID := '{5D50B637-4756-435A-816E-68ABFE86FC69}';
    ISOUrl := 'http://altd.embarcadero.com/download/radstudio/10/delphicbuilder10___upd1.iso';
    ISOMd5 := '34bf51b0f017541b8521e7efd2b6fbee';
  end;
  VerList.AddObject(RadStudioVersion^.Name, TObject(RadStudioVersion));

  New(RadStudioVersion);
  with RadStudioVersion^ do
  begin
    Name := 'Rad Studio 10 Seattle';
    Ver := '23.0.20618.2753';
    BDSVersion := '17.0';
    LicVerStr := '10 Seattle';
    LicHostPID := 8217;
    LicHostSKU := 53;
    LicDelphiPID := '2023';
    LicCBuilderPID := '4020';
    BdsPatchInfo.Crc := $59176E2B;
    BdsPatchInfo.Sha1 := '0f4255ee60dc860bdcf75c3358d03674757474a5';
    BdsPatchInfo.PatchOffset := $500EA;
    BdsPatchInfo.FinalizeArrayOffset := $225EC8;
    LicenseManagerPatchInfo.Crc := $D06C02B0;
    LicenseManagerPatchInfo.Sha1 := '4ff37906e7283448aecab34c73c8dbe3d45f55a6';
    LicenseManagerPatchInfo.PatchOffset := $1CA98A;
    LicenseManagerPatchInfo.FinalizeArrayOffset := $630634;
    mOasisRuntimePatchInfo.Sha1 := '7aa466dd1d2c685edd69ee41d1c8ebc1d2b56bb4';
    mOasisRuntimePatchInfo.PatchOffset := $00162CBD;
    SetupGUID := '{09FECC13-2950-4AE6-BB23-05C206979F18}';
    ISOUrl := 'http://altd.embarcadero.com/download/radstudio/10/delphicbuilder10.iso';
    ISOMd5 := '9d4bac568aced7f1f82d4a44124fb37c';
  end;
  VerList.AddObject(RadStudioVersion^.Name, TObject(RadStudioVersion));
end;

procedure FinallyRadStudioVersion(VerList: TStringList);
var
  RadStudioVersion: PRadStudioVersion;
begin
  while VerList.Count > 0 do
  begin
    RadStudioVersion := PRadStudioVersion(VerList.Objects[0]);
    if RadStudioVersion <> nil then
      Dispose(RadStudioVersion);
    VerList.Delete(0);
  end;
end;

initialization

RadStudioVersionList := TStringList.Create;
InitRadStudioVersion(RadStudioVersionList);

finalization

FinallyRadStudioVersion(RadStudioVersionList);
FreeAndNil(RadStudioVersionList);

end.
