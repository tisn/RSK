program RadStudioKeygen;

{$H-}
{$R *.dres}

uses
  Forms,
  MainFrm in 'MainFrm.pas' {FrmMain},
  RadVersion in 'RadVersion.pas',
  RadLicense in 'RadLicense.pas',
  SHFolderDll in 'SHFolderDll.pas',
  RadKeygen in 'RadKeygen.pas',
  FGInt in 'FGInt.pas',
  Sha1 in 'Sha1.pas',
  Vcl.Themes,
  Vcl.Styles,
  System.IO in 'System.IO.pas',
  FGInt2 in 'FGInt2.pas',
  superobject in 'superobject.pas',
  superdate in 'superdate.pas',
  supertimezone in 'supertimezone.pas',
  supertypes in 'supertypes.pas',
  superxmlparser in 'superxmlparser.pas',
  RTF in 'RTF.pas';

{$R 'UAC.res' 'UAC.rc'}
{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Sky');
  Application.Title := 'Rad Studio Keygen';
  Application.CreateForm(TFrmMain, FrmMain);
  Application.Run;
end.
